//
// tx-only
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/pgmspace.h>
#include <compat/deprecated.h>
#include "helper.h"

#if !defined(CTC0)
#    define CTC0 WGM01
#endif

extern volatile uint32_t ticks;

// tx_only_init()
// Sets timer0 mode and count to simulate a baud rate.
void tx_only_init(uint8_t timer0_mode,
		  uint8_t timer0_count);

// tx_only_clear()
// Clears the transmit buffer.
void tx_only_clear();

// tx_only_chr()
// Places a character in the transmit buffer.  Returns false if the
// buffer is full.
bool tx_only_chr(uint8_t c);

// tx_only_str
// Places a string in the transmit buffer.  Returns false if the
// buffer is full.
bool tx_only_str(const uint8_t* str);
bool tx_only_str_P(PGM_P str);

// You should define the following two functions in your own code:
//
//     tx_only_port_low()
//     tx_only_port_high()
//
// If you do not, you will get the following definitions that correspond
// to the UART transmit line on an ATMEGA103:
//     
//     void tx_only_port_low()
//     {
//         sbi(PORTE, 1);
//     }
//     
//     void tx_only_port_high()
//     {
//         cbi(PORTE, 1);
//     }
