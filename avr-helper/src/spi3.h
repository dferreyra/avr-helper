//
// spi3
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#ifndef spi3_included
#define spi3_included 1

#include "helper.h"

void spi3_init(void);

// spi3_clock_data
//
// Clock data on SPI.
//
// Returns false if there is not enough room in the queue.
//
// The memory pointed to by data must remain valid
// until all the data is clocked on SPI.
bool spi3_clock_data(uint8_t* data,
		     uint8_t size,
		     void (*pre)(void),
		     void (*post)(void),
		     volatile bool* done);

// spi3_pend_data
//
// Add the data to the queue, but don't clock it on the SPI until
// spi3_clock_data() is called.  To clear any pending items on the queue,
// call spi3_clear_pending().
//
// Returns false if there is not enough room in the queue.
//
// The memory pointed to by data must remain valid
// until all the data is clocked on SPI.
//
// Use this function to make atomic additions to the queue.
bool spi3_pend_data(uint8_t* data,
		    uint8_t size,
		    void (*pre)(void),
		    void (*post)(void),
		    volatile bool* done);

// spi3_clear_pending
//
// Clears any pending items on the queue.
void spi3_clear_pending(void);

#endif // #ifndef spi3_included
