//
// tx-only
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include <limits.h>
#include <string.h>
#include "tx-only.h"

static volatile uint8_t state;

#define TX_BUFFER_SIZE 64
static volatile uint8_t tx_buffer[TX_BUFFER_SIZE];
static volatile uint8_t tx_pop = 0;
static volatile uint8_t tx_free = TX_BUFFER_SIZE;
static uint8_t tx_push = 0;

void tx_only_port_low(void);
void tx_only_port_high(void);

#if defined(OCIE0)
void tx_only_init(uint8_t timer0_mode,
		  uint8_t timer0_count)
{
    // Stop bit mode.
    tx_only_port_low();

    // Setup timer 0 mode.  Include reset on overflow bit.
    timer0_mode &= 0x07;
    outp(inp(TCCR0) | _BV(CTC0) | timer0_mode, TCCR0);

    outp(timer0_count, OCR0);

    outp(0, TCNT0);

    // Enable timer compare interrupt.
    sbi(TIMSK, OCIE0);
}
#endif

void tx_only_clear()
{
    cli();
    tx_pop = 0;
    tx_push = 0;
    tx_free = TX_BUFFER_SIZE;
    sei();
}

inline
static void push_char(uint8_t c)
{
    tx_buffer[tx_push++] = c;
    if (tx_push == TX_BUFFER_SIZE)
	tx_push = 0;
}

bool tx_only_chr(uint8_t c)
{
    cli();
    if (! tx_free) {
	sei();
	return false;
    }
    tx_free--;
    sei();

    push_char(c);

    cli();
    if (! state)
	state = 1;
    sei();
    return true;
}

bool tx_only_str(const uint8_t* str)
{
    uint8_t len = strlen((const char*)str);
    cli();
    if (len > tx_free) {
	sei();
	return false;
    }
    tx_free -= len;
    sei();

    while (*str)
	push_char(*str++);

    cli();
    if (! state)
	state = 1;
    sei();
    return true;
}

bool tx_only_str_P(PGM_P str)
{
    uint8_t len = 0;
    uint8_t c;

    while ((c = pgm_read_byte(str + len)))
	++len;
    cli();
    if (len > tx_free) {
	sei();
	return false;
    }
    tx_free -= len;
    sei();

    while ((c = pgm_read_byte(str++)))
	push_char(c);

    cli();
    if (! state)
	state = 1;
    sei();
    return true;
}

#define HIGH 0
#define LOW  1
#define START_BIT HIGH
#define STOP_BIT  LOW

volatile uint32_t ticks;

#if defined(OCIE0)
SIGNAL(SIG_OUTPUT_COMPARE0)
{
    static uint8_t data;
    static uint8_t next_bit = STOP_BIT;

    if (next_bit)
	tx_only_port_low();
    else
	tx_only_port_high();

    ++ticks;

    if (state == 0) {
	// No data.
    } else if (state == 1) {
	// Start bit.
	next_bit = START_BIT;

	// Pop character and transmit it.
	data = tx_buffer[tx_pop++];
	if (tx_pop == TX_BUFFER_SIZE)
	    tx_pop = 0;
	tx_free++;

	state++;
    } else if (state == 10) {
	// Stop bit.
	next_bit = STOP_BIT;

	if (tx_free == TX_BUFFER_SIZE)
	    state = 0;
	else
	    state = 1;
    } else {
	// Data bits.
	next_bit = data & 0x01;
	data >>= 1;
	state++;
    }
}
#endif
