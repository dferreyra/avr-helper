//
// spi3-no-isr
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <spi3-no-isr.h>
#include <compat/deprecated.h>

extern void spi3_no_isr_wait(void);

void spi3_no_isr_init(void)
{
    // Put SPI into master mode.
    sbi(SPCR, MSTR);
    // Enable SPI.
    sbi(SPCR, SPE);
}

bool spi3_no_isr_clock_data(uint8_t* data,
                            uint8_t size,
                            void (*pre)(void),
                            void (*post)(void),
                            volatile bool* done)
{
    return spi3_no_isr_pend_data(data, size, pre, post, done);
}

bool spi3_no_isr_pend_data(uint8_t* data,
                           uint8_t size,
                           void (*pre)(void),
                           void (*post)(void),
                           volatile bool* done)
{
    if (pre)
        pre();
    while (size--) {
        SPDR = *data;
        spi3_no_isr_wait();
        *data++ = SPDR;
    }
    if (post)
        post();
    if (done)
        *done = true;
    return true;
}

void spi3_no_isr_clear_pending(void)
{
}
