//
// uart-tx-str-p
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>
#include "uartx.h"
#include "uart-defx.h"
#include "uart-tx-inline.h"

bool PORT_SUFFIX(uart_tx_str_P)(PGM_P str)
{
    uint8_t len = 0;
    uint8_t c;

    while ((c = pgm_read_byte(str + len)))
	++len;
    cli();
    if (len > PORT_SUFFIX(uart_tx_free)) {
	sei();
	return false;
    }
    PORT_SUFFIX(uart_tx_free) -= len;

    while ((c = pgm_read_byte(str++)))
	PORT_SUFFIX(push_char)(c);

    sbi(UCR, UDRIE);
    sei();
    return true;
}
