//
// spi3
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <compat/deprecated.h>
#include "spi3.h"

typedef struct {
    uint8_t* data;
    uint8_t size;
    void (*pre)(void);
    void (*post)(void);
    volatile bool* done;
} queue_element;

#define QUEUE_SIZE 24

static volatile queue_element queue[QUEUE_SIZE];
static uint8_t queue_push;
static volatile uint8_t queue_pop;
static volatile uint8_t queue_free = QUEUE_SIZE; // Change in cli/sei block.
static volatile bool in_progress = false;        // Change in cli/sei block.

static uint8_t queue_push_pending;
static volatile uint8_t queue_free_pending = QUEUE_SIZE; // Change in cli/sei block.

void spi3_init(void)
{
    // Put SPI into master mode.
    sbi(SPCR, MSTR);
    // Enable SPI.
    sbi(SPCR, SPE);
    // Enable SPI interrupt.
    sbi(SPCR, SPIE);
}

bool spi3_clock_data(uint8_t* data,
		     uint8_t size,
		     void (*pre)(void),
		     void (*post)(void),
		     volatile bool* done)
{
    if (! spi3_pend_data(data, size, pre, post, done))
	return false;
    queue_push = queue_push_pending;

    cli();
    queue_free = queue_free_pending;
    if (! in_progress) {
	volatile queue_element* qe = queue + queue_pop;
	if (qe->pre)
	    qe->pre();
	SPDR = *qe->data;
	in_progress = true;
    }
    sei();
    return true;
}

bool spi3_pend_data(uint8_t* data,
		    uint8_t size,
		    void (*pre)(void),
		    void (*post)(void),
		    volatile bool* done)
{
    volatile queue_element* qe;

    cli();
    if (queue_free_pending == 0) {
	sei();
	return false;
    }
    --queue_free_pending;
    sei();

    qe = queue + queue_push_pending;
    qe->data = data;
    qe->size = size;
    qe->pre = pre;
    qe->post = post;
    qe->done = done;

    if (++queue_push_pending == QUEUE_SIZE)
	queue_push_pending = 0;
    return true;
}

void spi3_clear_pending(void)
{
    queue_push_pending = queue_push;
    cli();
    queue_free_pending = queue_free;
    sei();
}

SIGNAL(SIG_SPI)
{
    volatile queue_element* qe = queue + queue_pop;
    // Retrieve spi reply.
    *qe->data++ = SPDR;
    if (--qe->size) {
        // Send next character of same element
	SPDR = *qe->data;
    } else {
        // Finish this element
	if (qe->post)
	    qe->post();
	if (qe->done)
	    *qe->done = true;
	if (++queue_pop == QUEUE_SIZE)
	    queue_pop = 0;
	++queue_free_pending;
        // Move on to next element
	if (++queue_free != QUEUE_SIZE) {
	    qe = queue + queue_pop;
	    if (qe->pre)
		qe->pre();
	    SPDR = *qe->data;
	} else
	    in_progress = false;
    }
}
