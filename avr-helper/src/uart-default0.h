//
// uart-default0
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include "uart0.h"

#define uart_rx_init uart_rx_init0
#define uart_rx_init_no_isr uart_rx_init_no_isr0
#define uart_rx_clear uart_rx_clear0
#define uart_rx_char uart_rx_char0
#define uart_tx_init uart_tx_init0
#define uart_tx_init_no_isr uart_tx_init_no_isr0
#define uart_tx_clear uart_tx_clear0
#define uart_tx_char uart_tx_char0
#define uart_tx_str uart_tx_str0
#define uart_tx_str_P uart_tx_str_P0
#define uart_tx_str_partial uart_tx_str_partial0
#define uart_tx_str_partial_P uart_tx_str_partial_P0
