//
// ltoa
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <stdlib.h>
#include "helper.h"

char* my_ltoa(int32_t value, char* buffer, int8_t radix)
{
    // Generate number backwards.
    uint32_t uvalue;
    bool neg;
    char *eptr = buffer;

    if (radix < 2 || radix > 36)
	radix = 10;

    if (value < 0 && radix != 2 && radix != 16) {
	uvalue = -value;
	neg = true;
    } else {
	uvalue = value;
	neg = false;
    }
    do {
	uint8_t v = uvalue % radix;
	if (v >= 10)
	    *eptr++ = 'A' + (v - 10);
	else
	    *eptr++ = '0' + v;
	uvalue /= radix;
    } while (uvalue);
    if (neg)
	*eptr++ = '-';

    // Reverse it.
    *eptr-- = 0;

    char *bptr = buffer;
    while (bptr < eptr) {
	char c = *eptr;
	*eptr-- = *bptr;
	*bptr++ = c;
    }
    return buffer;
}
