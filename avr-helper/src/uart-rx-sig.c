//
// uart-rx-sig
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/interrupt.h>
#include "uartx.h"
#include "uart-defx.h"

void PORT_SUFFIX(uart_rx_init)(void)
{
    // Enable receive line and receive interrupt.
    sbi(UCR, RXEN);
    sbi(UCR, RXCIE);
}

SIGNAL(SIG_UART_RECV)
{
    PORT_SUFFIX(uart_rx_buffer)[PORT_SUFFIX(uart_rx_push)++] = inp(UDR);
    if (PORT_SUFFIX(uart_rx_push) == PORT_SUFFIX(uart_rx_buffer_size))
	PORT_SUFFIX(uart_rx_push) = 0;
    // Disable receive interrupt if buffer is full.
    if (! --PORT_SUFFIX(uart_rx_free))
	cbi(UCR, RXCIE);
}
