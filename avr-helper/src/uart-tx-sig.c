//
// uart-tx-sig
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/interrupt.h>
#include "uartx.h"
#include "uart-defx.h"

void PORT_SUFFIX(uart_tx_init)(void)
{
    sbi(UCR, TXEN);
}

SIGNAL(SIG_UART_DATA)
{
    // Pop character and transmit it.
    outp(PORT_SUFFIX(uart_tx_buffer)[PORT_SUFFIX(uart_tx_pop)++], UDR);
    if (PORT_SUFFIX(uart_tx_pop) == PORT_SUFFIX(uart_tx_buffer_size))
	PORT_SUFFIX(uart_tx_pop) = 0;
    PORT_SUFFIX(uart_tx_free)++;

    // Disable data register empty interrupt.
    if (PORT_SUFFIX(uart_tx_free) == PORT_SUFFIX(uart_tx_buffer_size))
	cbi(UCR, UDRIE);
}
