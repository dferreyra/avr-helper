
\documentclass{report}
\usepackage{tex2page}
\usepackage{makeidx}
\usepackage{alltt}
\usepackage{xspace}

% tex2page definitions

\ifx\shipout\UnDeFiNeD % HTML only 
  % Choose png format for images
  \def\TZPimageformat{png}
  \imgpreamble 
    \usepackage{xspace}
  
    \let\LaTeXdocument\document 
    \def\document{\LaTeXdocument\LARGE} 
    \def\sub#1{_#1}
    \def\subrm#1{_\mathrm{#1}}
  \endimgpreamble 
  \let\TZPtexlayout=1
  \let\TZPusenontexlayoutnavigation=1
  \let\TZPsuppressheadinglinks=1
\else
  \newcommand{\mailto}[1]{#1}
\fi

% Reset underscore to be a regular character.
% Use \sub{} and \subrm{} to use math subscripting.
\newcommand{\sub}[1]{_#1}
\newcommand{\subrm}[1]{_\mathrm{#1}}
\catcode`\_=11

\newcommand{\betam}{$\beta$\xspace}

% Some helpers in case I want to change their format
\newcommand{\AVR}{AVR\xspace}
\newcommand{\AVRGCC}{\AVR GCC\xspace}
\newcommand{\AVRHelperLibrary}{\AVR Helper Library\xspace}
\newcommand{\WinAVR}{WinAVR\xspace}
\newcommand{\ISR}{interrupt service routine\xspace}

% Commands to help with formatting
\newcommand{\lit}[1]{\texttt{#1}}
\newcommand{\litindex}[1]{\index{#1@\lit{#1}}}
\newcommand{\funref}[1]{\ref{#1}}
\newcommand{\result}[1]{\lit{#1}}
\newcommand{\argument}[1]{\lit{#1}}
\newcommand{\reg}[1]{\lit{#1}\litindex{#1}}
\newcommand{\code}[1]{\lit{#1}}
\newenvironment{argdesc}{\begin{quote}\begin{description}}{\end{description}\end{quote}}

% Function entries
% Define (and re-define) \thisfunction to refer to the latest function in question
\newcommand{\thisfunction}{\emph{unknown function}}
\newcommand{\function}[3]{\subsubsection*{\lit{#1}\hspace*{\fill}\hbox{\emph{#2}}\xrtag{#3}{\lit{#3}}\litindex{#3}}\renewcommand{\thisfunction}{\lit{#3}\xspace}}

% Generate an index
\makeindex

\title{\AVRHelperLibrary 1.3}
\author{Dean Ferreyra\\\mailto{dean@octw.com}\\http://www.bourbonstreetsoftware.com/}

\begin{document}

\maketitle
\tableofcontents

\sloppy

\chapter{Introduction}

The \AVRHelperLibrary is a small collection of modules written in C
that I have found useful while developing and prototyping \AVR-based
software.  It includes a buffered USART module, buffered and
unbuffered SPI modules, and some odds and ends like ``gentle'' EEPROM
update functions, a thermistor temperature calculation function, and
``endian'' conversion functions.

I have released this code under the
\urlh{http://www.gnu.org/copyleft/lesser.html}{GNU Lesser General
  Public License} with the hope that others might find it useful.

\chapter{Build and Installation}

To build and install the \AVRHelperLibrary, you need to have already
installed the \AVRGCC cross-compiler.

\section{Build}
\index{building from source}

To install the \AVRHelperLibrary, unpack the source tree.  If you
downloaded the \lit{*.tgz} version, you can do this with the following
command:
\begin{verbatim}
  $ tar -xzf helper-1.3-src.tgz
\end{verbatim} % $ (Placed here to help Emacs get the formatting right.)

Next, change directory to the head of the source tree that was just
unpacked and run the \code{make} command:
\begin{verbatim}
  $ cd helper-1.3
  $ make
\end{verbatim}
This should build the \AVRHelperLibrary for all the supported \AVR
microcontrollers.

\section{Installation}
\index{installation}

Once the libraries have been successfully built, you need to install
the library components to a location where the \AVRGCC compiler can
find them.

The default installation directory prefix is \code{/usr/local}.  If
you wish to change this, you will have to edit the \code{src/Makefile}
file in the source tree.  In the Makefile, you'll find a line that
looks like this:
\begin{verbatim}
  prefix = /usr/local
\end{verbatim}
Change this line to desired directory prefix. For example, if you're
using \urlh{http://winavr.sourceforge.net/}{\WinAVR}, you will need to
change this this point to where \WinAVR has been installed.  For
example, if \WinAVR has been installed in \verb{c:\WinAVR}, then
change the prefix line to this:
\begin{verbatim}
  prefix = c:/WinAVR
\end{verbatim}

Finally, from the head of the source tree, or from the \code{src}
directory, run this:
\begin{verbatim}
  $ make install
\end{verbatim} % $ (Placed here to help Emacs get the formatting right.)

\chapter{EEPROM}
\index{EEPROM}

This module provides ``gentle'' EEPROM writing routines to help extend
the life of the on-chip EEPROM.  Before each EEPROM data byte is
written, these functions read the byte from the EEPROM and only write
the new byte if the values are different.

\section{Usage}

To use the EEPROM writing functions, use this:
\begin{verbatim}
  #include <eeprom-gentle.h>
\end{verbatim}

\section{Functions}

\function{void gentle_eeprom_write_byte(uint8_t *addr, uint8_t val)}{function}{gentle_eeprom_write_byte()}

Writes a single byte \argument{val} to EEPROM address \argument{addr},
but only if the byte in the EEPROM is different.

\function{void gentle_eeprom_write_word(uint16_t *addr, uint16_t val)}{function}{gentle_eeprom_write_word()}

Writes the bytes of a single 16 bit word \argument{val} to EEPROM
address \argument{addr}, but only the bytes that are different than
what is currently found in the EEPROM.

\function{void gentle_eeprom_write_dword(uint16_t *addr, uint32_t val)}{function}{gentle_eeprom_write_dword()}

Writes the bytes of a single 32 bit word \argument{val} to EEPROM
address \argument{addr}, but only the bytes that are different than
what is currently found in the EEPROM.

\function{void gentle_eeprom_write_block(const void *buf, void *addr, size_t n)}{function}{gentle_eeprom_write_block()}

Writes \argument{n} bytes of the block pointed to by \argument{buf} to
EEPROM address \argument{addr}, but only the bytes that are different
than what is currently found in the EEPROM.

\chapter{USART}
\index{USART}

% Needed documentation:
%   Examples
%   How to provide your own rx and tx ISRs
%   How to modify the size of the rx and tx buffers

This module provides basic buffered USART support for Atmel \AVR
microcontrollers.  It support microcontrollers with 1 or 2 USART
ports.

This module separates the actions of receive and transmit into
independent parts.  This way, if you will only be receiving or only be
transmitting, you only need to link in and initialize the relevant
part.

\section{Usage}

To use the USART functions for microcontrollers that only support one
USART port, use this:
\begin{verbatim}
  #include <uart.h>
\end{verbatim}
and use the function names as shown below.

To use the USART functions for microcontrollers that support two USART
ports, use this:
\begin{verbatim}
  #include <uart0.h>
  #include <uart1.h>
\end{verbatim}
and append the digit \code{0} or \code{1} to the function names shown below
to pick the appropriate port.  For example, the following initializes
the transmit functions of USART port 1 and transmits a single character:

\begin{verbatim}
  uart_tx_init1();
  uart_tx_char1('x');
\end{verbatim}

\section{USART Receive Functions}

\function{void uart_rx_init(void)}{function}{uart_rx_init()}

This function initializes the receive part of this module.  This
entails enabling the USART receive line and enabling the USART receive
interrupt.  Also, it automatically links in the default receive \ISR 
from the library.

\function{void uart_rx_init_no_isr(void)}{inline}{uart_rx_init_no_isr}

This function is identical to \funref{uart_rx_init()}, but it does
\emph{not} link in the default receive \ISR.  In this case, you must
provide your own \ISR.  % See \ref{custom ISR} for the details.

\function{void uart_rx_clear(void)}{function}{uart_rx_clear()}

This function clears the receive buffer.

\function{int uart_rx_char(void)}{function}{uart_rx_char()}

This function takes a character from the receive buffer and returns
it.  If the receive buffer is empty, this function returns
\result{-1}.

\section{USART Transmit Functions}

\function{void uart_tx_init(void)}{function}{uart_tx_init()}

This function initializes the transmit part of this module.  This
entails enabling the USART transmit line.  Also, it automatically
links in the default transmit \ISR from the library.

\function{void uart_tx_init_no_isr(void)}{inline}{uart_tx_init_no_isr()}

This function is identical to \funref{uart_tx_init()}, but it does
\emph{not} link in the default transmit \ISR.  In this case, you must
provide your own \ISR.  % See \ref{custom ISR} for the details.

\function{void uart_tx_clear(void)}{function}{uart_tx_clear()}

This function clears the transmit buffer.

\function{bool uart_tx_char(char c)}{function}{uart_tx_char()}

If there is room in the transmit buffer, this function places the
given character, \argument{c}, into the transmit buffer.  If the
transmit interrupt is disabled, it enables the interrupt which starts
the transmission process.  If the transmit interrupt is already
enabled, a transmission is already in progress and the character will
eventually be transmitted.  The function then returns \result{true}.

If the transmit buffer is full when this function is called, it does
nothing and returns \result{false}.

\function{bool uart_tx_str(const char* str)}{function}{uart_tx_str()}

If there is room in the transmit buffer, this function places the
given string, \argument{str}, into the transmit buffer.  If the
transmit interrupt is disabled, it enables the interrupt which starts
the transmission process.  If the transmit interrupt is already
enabled, a transmission is already in progress and the character will
eventually be transmitted.  The function then returns \result{true}.

If the transmit buffer is full when this function is called, it does
nothing and returns \result{false}.

\function{bool uart_tx_str_P(PGM_P str)}{function}{uart_tx_str_P()}

This function is analogous to \funref{uart_tx_str()}, except that
instead of taking a string in RAM, \argument{str} points to a string
stored in the program space.

\function{const char* uart_tx_str_partial(const char* str)}{function}{uart_tx_str_partial()}

This function places into the transmit buffer as many characters from
the given string, \argument{str}, as will fit.

If any characters were placed into the transmit buffer, and the
transmit interrupt is disabled, it enables the interrupt which starts
the transmission process.  If the transmit interrupt is already
enabled, a transmission is already in progress and any characters
placed into the transmit buffer will eventually be transmitted.

If all the characters in \argument{str} were placed into the transmit
buffer, then this function returns \result{0}.  Otherwise, it returns
a pointer to the characters in the string that were not placed into
the transmit buffer.  This way, you can use the returned string to try
again latter.

\function{PGM_P uart_tx_str_partial_P(PGM_P str)}{function}{uart_tx_str_partial_P()}

This function is analogous to \funref{uart_tx_str_partial()}, except
that instead of taking a string in RAM, \argument{str} points to a
string stored in the program space.  Also, it returns a pointer to the
remaining characters stored in the program space.

\function{bool uart_tx_data(const char* data, uint8_t count)}{function}{uart_tx_data()}

This function is analogous to \funref{uart_tx_str()}, except that
instead of taking \verb+'\0'+ terminated string, it takes a character
pointer to data, \argument{data}, and the number of characters to
transmit, \argument{count}.  If all \argument{count} characters will
not fit in the transmit buffer, this function returns \result{false}.
Otherwise, it returns \result{true}.

\function{uint16_t uart_generate_ubrr(uint32_t fosc, uint32_t baud)}{function}{uart_generate_ubrr()}

This function calculates the closest \reg{UBRR} value for the given
oscillator frequency, \argument{fosc}, and the desired baud rate,
\argument{baud}.

\chapter{Software UART Transmission}
\index{UART, software-based}

This module provides buffered, software-based, transmit-only UART
functionality.  TIMER0 is the baud-rate generator.  By default, this
module twiddles \reg{PORTE} bit 1 or \reg{PORTD} bit 1.  By redefining
a couple of functions you can make any output bit the transmit line.

\section{Usage}

To use the transmit-only UART functions, place this include in your
source file:
\begin{verbatim}
  #include <tx-only.h>
\end{verbatim}

\section{Functions}

% \function{uint16_t divider_network_lower_resistor(uint16_t v_ad_max, uint16_t v_ad_counts, uint16_t r_upper)}{function}{divider_network_lower_resistor()}
\function{void tx_only_init(uint8_t timer0_mode, uint8_t timer0_count)}{function}{tx_only_init()}

This function initializes TIMER0 by placing it in the given mode,
\argument{timer0_mode}, and setting the counter value to
\argument{timer0_count}.  Finally the \reg{OCIE0} interrupt is
enabled.

Only the three low-order bits of the \argument{timer0_mode} argument
are used.  Register \reg{TCCR0} is updated with these bits, plus bit
\reg{CTC0}.  Register \reg{OCR0} is updated with \argument{timer0_count}.

For example, to simulate a 9600 baud UART transmission line on an
{ATmega103} running at 4 MHz, call \thisfunction like this:
\begin{verbatim}
  tx_only_init(0x02, 4000000 / 8 / 9600);
\end{verbatim}

\function{void tx_only_clear()}{function}{tx_only_clear()}

This function clears the transmit buffer.

\function{bool tx_only_chr(uint8_t c)}{function}{tx_only_chr()}

If there is room in the transmit buffer, this function places the
given character, \argument{c}, into the transmit buffer and the
function then returns \result{true}.  Otherwise, it returns
\result{false}.

\function{bool tx_only_str(const uint8_t* str)}{function}{tx_only_str()}

If there is room in the transmit buffer, this function places the
given string, \argument{str}, into the transmit buffer and returns
\result{true}.  Otherwise, it returns \result{false}.

\function{bool tx_only_str_P(PGM_P str)}{function}{tx_only_str_P()}

This function is analogous to \funref{tx_only_str()}, except that
instead of taking a string in RAM, \argument{str} points to a string
stored in the program space.

\function{void tx_only_port_low()}{optional user-defined function}{tx_only_port_low()}
\function{void tx_only_port_high()}{optional user-defined function}{tx_only_port_high()}

These two functions are provided by the programmer to twiddle the bits
of the transmission line.  The ``low'' and ``high'' that appear in
their names are in reference to the signal level, where ``low'' means
a negative voltage, and ``high'' means a positive voltage.

These functions are optional.  If you don't provide them, the library
supplies its own version that twiddles \reg{PORTE} bit 1 (or if the
microcontroller does not have \reg{PORTE}, then \reg{PORTD} bit 1).

\section{Caveats}

As written, this module has many limitations:
\begin{itemize}
  \item It supports only one UART.
  \item It is hard-coded to use TIMER0.
  \item All of the functions are linked into the image, even if
    they're not used.
  \item Because the \ISR calls two possibly user-defined functions,
    it is especially slow.
\end{itemize}

\chapter{Basic SPI}
\index{SPI}

This module provides a basic interface to the microcontroller SPI in
master mode.  It utilizes an \ISR to clock out the SPI data
asynchronously.

\section{Usage}

To use the SPI module, place the following in your source files:
\begin{verbatim}
  #include <spi2.h>
\end{verbatim}

\section{Functions}

\function{void spi2_init(void)}{function}{spi2_init()}

This function places the SPI into ``master'' mode, enables the SPI,
and enables the SPI interrupt.  The library provides the required
\ISR.

\function{bool spi2_busy(void)}{function}{spi2_busy()}

This function returns \result{true} if the SPI module is busy clocking
SPI data.  While the SPI module is busy, \funref{spi2_send_byte()} and
\funref{spi2_send_data()} will do nothing and will return
\result{false}.

\function{bool spi2_send_byte(uint8_t* b, volatile bool* done_flag)}{function}{spi2_send_byte()}

This function begins an asynchronous SPI exchange of one byte.  The
data to transmit is the byte pointed to by \argument{b}.  If
\argument{done_flag} is not \code{NULL}, when the exchange is
complete, the \ISR sets this flag to \code{true}.  After the exchange,
the byte pointed to by \argument{b} will contain the received byte.

While the module is busy exchanging the byte, it is important that the
location pointed to by \argument{b} remain valid.  For example, it
would be an error if \argument{b} points to a location on the stack
that goes out of scope while the exchange is taking place.

\function{bool spi2_send_data(uint8_t* d, uint8_t s, volatile bool* done_flag)}{function}{spi2_send_data()}

This function begins an asynchronous SPI exchange of \argument{s}
bytes of the data pointed to by \argument{d}.  If \argument{done_flag}
is not \code{NULL}, when the exchange is complete, the \ISR sets this
flag to \code{true}.  After the exchange, the data pointed to by
\argument{d} will contain the received data.

While the module is busy exchanging the byte, it is important that the
location pointed to by \argument{d} remain valid.  For example, it
would be an error if \argument{d} points location on the stack that
goes out of scope while the exchange is taking place.

\chapter{Buffered SPI}
\index{SPI}

This module provides a buffered interface to the microcontroller SPI
in master mode.  It utilizes an \ISR to clock out the SPI data
asynchronously.

Each data transfer request can include execution of a user-defined
function just before the transmission begins and just after the
transmission completes.  The user-defined functions can do things like
enable and disable chip-select lines and change SPI clocking
frequencies to allow you to buffer SPI transmissions for multiple SPI
devices.

\section{Usage}

To use the SPI module, place the following in your source files:
\begin{verbatim}
  #include <spi3.h>
\end{verbatim}

\section{Functions}

\function{void spi3_init(void)}{function}{spi3_init()}

This function places the SPI into ``master'' mode, enables the SPI,
and enables the SPI interrupt.  The library provides the required
\ISR.

\function{bool spi3_clock_data(uint8_t* data, uint8_t size, void (*pre)(void), void (*post)(void), volatile bool* done)}{function}{spi3_clock_data()}

This function buffers an SPI data transfer request.  A data transfer
request consists of:
\begin{argdesc}
  \item[\argument{data}] Pointer to the data to exchange.
  \item[\argument{size}] Number of bytes to exchange.
  \item[\argument{pre}]  Thunk called by the \ISR before the exchange begins.
  \item[\argument{post}] Thunk called by the \ISR after the exchange ends.
  \item[\argument{done}] Pointer to a Boolean set by the \ISR to
    \code{true} once the exchange ends and the call to \argument{post}
    completes.
\end{argdesc}
If there is room in the buffer, the request is buffered and
\thisfunction returns \result{true}.  Otherwise, \thisfunction returns
\result{false}.  The data is clocked onto the SPI bus once all
previously buffered requests are completed.  Any pending requests
placed in the buffer by calls to \funref{spi3_pend_data()} will no
longer be considered pending and they will all eventually be clocked
onto the SPI.

The arguments \argument{pre}, \argument{post}, and \argument{done} are
optional; passing a \code{NULL} causes \thisfunction to ignore that
argument.

Note that the memory pointed to by \argument{data} and \argument{done}
must remain valid until the exchange is complete.  For example, it
would be an error if \argument{data} were to point to a location on
the stack and that location were to go out of scope while the exchange
was taking place.

Note also that the \argument{pre} and \argument{post} thunks are called
directory from the \ISR.

\function{bool spi3_pend_data(uint8_t* data, uint8_t size, void (*pre)(void), void (*post)(void), volatile bool* done)}{function}{spi3_pend_data()}

This functions is similar to \funref{spi3_clock_data()}, except that
the data transfer request is held pending in the buffer without being
clocked onto the SPI bus.  This function can be called repeatedly to
buffer more requests.  Pending transfer requests will begin being
clocked onto the SPI the next time \funref{spi3_clock_data()} is
called.

This function is useful when a multi-byte exchange needs to take place
without interruption, but when it is more convenient to build the
multi-byte exchange in pieces.

\function{void spi3_clear_pending(void)}{function}{spi3_clear_pending()}

This function clears any pending requests that have been placed in the
buffer by \funref{spi3_pend_data()}.

\section{Unbuffered Interface}
\litindex{spi3_no_isr_init()}
\litindex{spi3_no_isr_clock_data()}
\litindex{spi3_no_isr_pend_data()}
\litindex{spi3_no_isr_clear_pending()}

There is also an \emph{unbuffered} version of this interface that does
not use an \ISR.  To use the unbuffered version, place the following in your
source files:
\begin{verbatim}
  #include <spi3-no-isr.h>
\end{verbatim}
Every function from the buffered interface is available, but each
function is prefixed with \code{spi3_no_isr} instead of \code{spi3}.

Because the data is not buffered, \code{spi3_no_isr_clock_data()} and
\code{spi3_no_isr_pend_data()} both immediately clock out the data to
the SPI bus.

\function{void spi3_no_isr_wait(void)}{user-defined function}{spi3_no_isr_wait()}

This user defined function is called by
\code{spi3_no_isr_clock_data()} and \code{spi3_no_isr_pend_data()}
before placing the next byte into \reg{SPDR}.  At the very least, it
should wait for the \reg{SPIF} flag in \reg{SPSR} to be set, like
this:
\begin{verbatim}
void spi3_no_isr_wait(void)
{
    while (! (SPSR & _BV(SPIF)))
        ;
}
\end{verbatim}
By having the wait function be a user-defined function, you can
program in an escape or a timeout check, something like this:
\begin{verbatim}
void spi3_no_isr_wait(void)
{
    while (! (SPSR & _BV(SPIF)))
        check_timeout();
}
\end{verbatim}

\chapter{Resistor Divider Networks}
\index{resistor divider networks}

This module provides functions to calculate values from resistor
divider networks.

\section{Usage}

To use the resistor divider network functions, use this:
\begin{verbatim}
  #include <resistor-network.h>
\end{verbatim}

\section{Functions}

\function{uint16_t divider_network_lower_resistor(uint16_t v_ad_max, uint16_t v_ad_counts, uint16_t r_upper)}{function}{divider_network_lower_resistor()}

This function calculates the resistance of the lower resistor of a
resistor divider network, given the maximum A/D counts
\argument{v_ad_max}, the measured A/D counts \argument{v_ad_counts}
for the divider network, and the known resistance \argument{r_upper}
in ohms of the upper resistor.
It uses the equation
$$R\subrm{lower} = R\subrm{upper}
                   \times \frac{V\subrm{measured}}
                               {V\subrm{max} - V\subrm{measured}}$$
to arrive at the result.

\function{uint16_t divider_network_voltage(uint16_t v_ad_counts, uint16_t r_upper, uint16_t r_lower)}{function}{divider_network_voltage()}

This function calculates the voltage feeding into a resistor network
given the measured A/D counts \argument{v_ad_counts}, and the the
known resistances in ohms of the upper resistor \argument{r_upper} and
the lower resistor \argument{r_lower}.
It uses the equation
$$V\subrm{network} = V\subrm{measured}
                     \times \frac{R\subrm{upper} + R\subrm{lower}}
                                 {R\subrm{lower}}$$
to arrive at the result.

\chapter{Thermistor}
\index{thermistor}

This module provides a function to calculate the temperature of a
thermistor using an exponential model for NTC (negative temperature
coefficient)\index{NTC (negative temperature coefficient)}
thermistors.

\section{Usage}

To use the thermistor function, use this:
\begin{verbatim}
  #include <thermistor.h>
\end{verbatim}

\section{Functions}

\function{float thermistor_t2_at_r2(float beta, float t1, float r_t1, float r_t2)}{function}{thermistor_t2_at_r2()}

This function uses an exponential model for NTC thermistors to
approximate the thermistor's temperature from its resistance.  This
function returns a thermistor's temperature given the thermistor's
\betam value, \argument{beta}, the nominal resistance,
\argument{r_t1}, at the nominal temperature, \argument{t1}, and the
measured resistance, \argument{r_t2}.  The temperatures must be in an
absolute temperature scale; e.g., Kelvin.  The equation used is:
  $$T = \left[\frac{\ln\left(R/R\sub0\right)}{\beta} + \frac{1}{T\sub0}\right]^{-1}$$

\section{Caveat}

Generally, this model is not as accurate as a table lookup can be.
The measured temperature should fall within the range that the \betam
value was calculated for, and if you have a choice of \betam values,
you should choose the \betam value calculated over the smallest range
that still suits your expected temperature range.

Also, this module uses floating calculations and \code{log()} to
perform its calculation.  The floating-point library is large and
floating point calculations can be relatively slow.


\chapter{Network Order}
\index{network order}

This module provides functions to convert from the native byte order
on the \AVR microcontroller to a big-endian network order like the
order used by the Internet Protocol\index{Internet Protocol}.

\section{Usage}

To use the network order module, place the following in your source
files:
\begin{verbatim}
  #include <network.h>
\end{verbatim}

\section{Functions}

\function{uint16_t htons(uint16_t host)}{function}{htons()}

This function takes a 16 bit word in host order, \argument{host}, and
returns it in network order.

\function{uint32_t htonl(uint32_t host)}{function}{htonl()}

This function takes a 32 bit word in host order, \argument{host}, and
returns it in network order.

\function{uint16_t ntohs(uint16_t network)}{function}{ntohs()}

This function takes a 16 bit word in network order,
\argument{network}, and returns it in host order.

\function{uint32_t ntohl(uint32_t network)}{function}{ntohl()}

This function takes a 32 bit word in network order,
\argument{network}, and returns it in host order.

\function{uint16_t ntohb2(const uint8_t* network2)}{function}{ntohb2()}

This function takes a pointer to two bytes in network order,
\argument{network2}, and returns its value in host order.

\function{void htonb2(uint16_t host, uint8_t* network2)}{function}{htonb2()}

This functions takes a 16 bit word in host order, \argument{host}, and
writes its value in network order at the two bytes pointed to by
\argument{network2}.

\function{uint32_t ntohb3(const uint8_t* network3)}{function}{ntohb3()}

This function takes a pointer to three bytes in network order,
\argument{network3}, and returns its value in host order.

\function{void htonb3(uint32_t host, uint8_t* network3)}{function}{htonb3()}

This functions takes a 32 bit word in host order, \argument{host}, and
writes its value in network order at the three bytes pointed to by
\argument{network3}.  Only the least significant 24 bits of
\argument{host} are used in this conversion.

\function{uint32_t ntohb4(const uint8_t* network4)}{function}{ntohb4()}

This function takes a pointer to four bytes in network order,
\argument{network4}, and returns its value in host order.

\function{void htonb4(uint32_t host, uint8_t* network4)}{function}{htonb4()}

This functions takes a 32 bit word in host order, \argument{host}, and
writes its value in network order at the four bytes pointed to by
\argument{network4}.

% Place index on its own page
\newpage
\printindex

\end{document}
