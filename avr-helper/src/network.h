//
// network
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

uint16_t htons(uint16_t host);
uint32_t htonl(uint32_t host);
uint16_t ntohs(uint16_t network);
uint32_t ntohl(uint32_t network);

uint16_t ntohb2(const uint8_t* network2);
void htonb2(uint16_t host, uint8_t* network2);
uint32_t ntohb3(const uint8_t* network3);
void htonb3(uint32_t host, uint8_t* network3);
uint32_t ntohb4(const uint8_t* network4);
void htonb4(uint32_t host, uint8_t* network4);
