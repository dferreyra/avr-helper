//
// resistor-network
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#ifndef helper_resistor_network_included
#define helper_resistor_network_included 1

// Given measured A/D counts and the resistance in ohms of the upper
// resistor, return the value of the lower resistor.
uint16_t divider_network_lower_resistor(uint16_t v_ad_max,
                                        uint16_t v_ad_counts,
                                        uint16_t r_upper);

// 
uint16_t divider_network_voltage(uint16_t v_ad_counts,
                                 uint16_t r_upper,
                                 uint16_t r_lower);

#endif // #ifndef helper_resistor_network_included
