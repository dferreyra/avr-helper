//
// eeprom-gentle
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <avr/io.h>
#include <avr/eeprom.h>
#include "eeprom-gentle.h"

void gentle_eeprom_write_byte(uint8_t *addr, uint8_t val)
{
    uint8_t old_val = eeprom_read_byte(addr);
    if (old_val != val)
        eeprom_write_byte(addr, val);
}

void gentle_eeprom_write_word(uint16_t *addr, uint16_t val)
{
    gentle_eeprom_write_block(&val, addr, sizeof(val));
}

void gentle_eeprom_write_dword(uint16_t *addr, uint32_t val)
{
    gentle_eeprom_write_block(&val, addr, sizeof(val));
}

void gentle_eeprom_write_block(const void *buf, void *addr, size_t n)
{
    uint8_t* addr_byte = (uint8_t*)addr;
    const uint8_t* buf_byte = (const uint8_t*)buf;
    while (n--)
        gentle_eeprom_write_byte(addr_byte++, *buf_byte++);
}
