//
// uartx
// AVR serial communication routines.
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <stdbool.h>
#include <inttypes.h>
#include <avr/pgmspace.h>
#include <compat/deprecated.h>

#if ! defined(PORT_N)
#    define PORT_SUFFIX(id) id
#    if !defined(UCR)
#        define UCR UCSRB
#        define USR UCSRA
#    endif
#else
#    define PORT_SUFFIX_N(id, n) id ## n
#    define PORT_INFIX_N(id1, n, id2) id1 ## n ## id2
#    if PORT_N == 0
#        define PORT_SUFFIX(id) PORT_SUFFIX_N(id, 0)
#        define PORT_INFIX(id1, id2) PORT_INFIX_N(id1, 0, id2)
#    elif PORT_N == 1
#        define PORT_SUFFIX(id) PORT_SUFFIX_N(id, 1)
#        define PORT_INFIX(id1, id2) PORT_INFIX_N(id1, 1, id2)
#    else
#        error("Given PORT_N not supported")
#    endif

#    define SIG_UART_RECV PORT_INFIX(SIG_UART, _RECV)
#    define SIG_UART_DATA PORT_INFIX(SIG_UART, _DATA)

#    define UCR PORT_INFIX(UCSR, B)
#    define UDR PORT_SUFFIX(UDR)
#endif

// uart_rx_init()
// Enables the receive line and enables the receive interrupt and
// links in default ISR.
void PORT_SUFFIX(uart_rx_init) (void);

// uart_rx_init_no_isr()
// Enables the receive line and enables the receive interrupt.
inline
static void PORT_SUFFIX(uart_rx_init_no_isr)(void)
{
    // Enable receive line and receive interrupt.
    sbi(UCR, RXEN);
    sbi(UCR, RXCIE);
}

// uart_rx_clear()
// Clears the receive buffer.
void PORT_SUFFIX(uart_rx_clear)(void);

// uart_rx_char()
// Gets a character.  Returns -1 if receive buffer is empty.
int PORT_SUFFIX(uart_rx_char)(void);

// uart_tx_init()
// Enables the transmit line and links in default ISR.
void PORT_SUFFIX(uart_tx_init)(void);

// uart_tx_init_no_isr()
// Enables the transmit line.
inline
static void PORT_SUFFIX(uart_tx_init_no_isr)(void)
{
    sbi(UCR, TXEN);
}

// uart_tx_clear()
// Clears the transmit buffer.
void PORT_SUFFIX(uart_tx_clear)(void);

// uart_tx_char()
// Places a character in the transmit buffer.  Returns false if the
// buffer is full.
bool PORT_SUFFIX(uart_tx_char)(char c);

// uart_tx_str
// Places a string in the transmit buffer.  Returns false if the
// buffer is full.
bool PORT_SUFFIX(uart_tx_str)(const char* str);
bool PORT_SUFFIX(uart_tx_str_P)(PGM_P str);

// uart_tx_str_partial
// Places as many characters as it can into the transmit buffer.
// Returns zero if they were all transmitted, or a pointer to the
// next character to transmit if there was not enough free space in the buffer.
const char* PORT_SUFFIX(uart_tx_str_partial)
  (const char* str);
PGM_P PORT_SUFFIX(uart_tx_str_partial_P)(PGM_P str);

// uart_tx_data
// Places data in the transmit buffer.  Returns false if the
// buffer is full.
bool PORT_SUFFIX(uart_tx_data)(const unsigned char* data, uint8_t count);

// uart_generate_ubrr
// Calculate the UBRR value given the oscillator frequency and the
// desired baud rate.
uint16_t uart_generate_ubrr(uint32_t fosc, uint32_t baud);
