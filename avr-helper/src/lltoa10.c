//
// lltoa10
//
// Copyright 2000, 2001, 2002, 2003, 2004 Dean Ferreyra
//
// $Id$
// Dean Ferreyra

/*
This file is part of AVR-HELPER.

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 2.1 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public
License along with this library; see the file COPYING.LIB.  If not,
write to the Free Software Foundation, Inc., 59 Temple Place, Suite
330, Boston, MA 02111-1307  USA

Contact information:

Dean Ferreyra
12902 Malena Drive
Santa Ana, CA 92705-1102  USA

dean@octw.com
*/

#include <stdlib.h>
#include "helper.h"

void lltoa10(int64_t value, char* buffer)
{
    // Generate number backwards.
    bool neg = value < 0;
    char *bptr = buffer;
    do {
	*bptr++ = '0' + abs(value % 10);
	value /= 10;
    } while (value);
    if (neg)
	*bptr++ = '-';
    *bptr = 0;

    // Reverse it.
    bptr--;
    while (bptr > buffer) {
	char c = *bptr;
	*bptr-- = *buffer;
	*buffer++ = c;
    }
}
